﻿
using UnityEngine;


public enum PlayerAction {
  MoveUp,
  MoveDown,
  MoveLeft,
  MoveRight,
  ShootMain,
  ShootAlt,
  None
}

public interface IInputProvider {
  PlayerAction currentAction { get; }

  bool playerSelectedObject(out GameObject obj);

  Vector2 playerClickLocation();
}
