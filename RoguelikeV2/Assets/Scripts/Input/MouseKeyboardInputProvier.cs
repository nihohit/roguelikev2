﻿using UnityEngine;

public class MouseKeyboardInputProvier : IInputProvider {
  private RaycastHit2D hit;

  public MouseKeyboardInputProvier() {
    hit = new RaycastHit2D();
  }

  public PlayerAction currentAction { get {
      if (!Input.anyKey) {
        return PlayerAction.None;
      }

      if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) {
        return PlayerAction.MoveUp;
      }
      if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
        return PlayerAction.MoveLeft;
      }
      if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
        return PlayerAction.MoveRight;
      }
      if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) {
        return PlayerAction.MoveDown;
      }
      if (Input.GetMouseButton(0)) {
        return PlayerAction.ShootMain;
      }
      if (Input.GetMouseButton(1)) {
        return PlayerAction.ShootAlt;
      }

      return PlayerAction.None;
    }
  }

  public bool playerSelectedObject(out GameObject obj) {
    var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    hit = Physics2D.Raycast(mousePosition, Vector2.zero);
    obj = hit.collider?.gameObject;
    return hit != null;
  }

  public Vector2 playerClickLocation() {
    return Camera.main.ScreenToWorldPoint(Input.mousePosition);
  }
}
