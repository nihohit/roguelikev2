﻿using UnityEngine;
using Assets.Scripts.UnityBase;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

public class BoardScript : MonoBehaviour {
  public GameObject maskerObject;

  private UIMaskingScript maskingScript;
  private GameObject[,] gameObjectsBoard;
  private Dictionary<GameObject, Vector2Int> objectsToArrayIndices;
  private LevelModel levelModel;
  private IInputProvider inputProvider;
  private GameObject player;
  private bool ignoreInput;
  private float air;

  // Use this for initialization
  void Start() {
    maskingScript = maskerObject.GetComponent<UIMaskingScript>();
    inputProvider = new MouseKeyboardInputProvier();
    const int kBoardSize = 9;
    player = GameObject.Find("Player");
    setLevelModel(createBoardModel(kBoardSize));
    gameObjectsBoard = createBoardFromModel(levelModel, this.transform);
    objectsToArrayIndices = objectMappingFromArray(gameObjectsBoard);
    player.transform.position = centerOfTile(gameObjectsBoard, levelModel.Player.coords);
    air = 1;
    StartCoroutine(consumeAir());
  }

  private IEnumerator consumeAir() {
    while(true) {
      air -= 0.001f;
      maskingScript.UpdateAir(1 - air);
      yield return new WaitForSeconds(0.01f);
    }
  }

  private Dictionary<GameObject, Vector2Int> objectMappingFromArray(GameObject[,] board) {
    var dict = new Dictionary<GameObject, Vector2Int>();
    for (int i = 0; i < board.GetLength(0); i++) {
      for (int j = 0; j < board.GetLength(1); j++) {
        dict[board[i, j]] = new Vector2Int(i, j);
      }
    }
    return dict;
  }

  static LevelModel createBoardModel(int boardSize) {
    var generator = new ConstantMapGenerator();
    return generator.boardModel(boardSize);
  }

  static GameObject[,] createBoardFromModel(LevelModel levelModel, Transform parent) {
    var mapInstantiator = new SuckyMapInstantiator();
    return mapInstantiator.creatObjectsForModel(levelModel, parent.gameObject);
  }

  // Update is called once per frame
  void Update() {
    if (ignoreInput) {
      return;
    }

    switch (inputProvider.currentAction) {
      case PlayerAction.MoveDown:
      case PlayerAction.MoveLeft:
      case PlayerAction.MoveRight:
      case PlayerAction.MoveUp:
        move();
        break;
      case PlayerAction.ShootMain:
      case PlayerAction.ShootAlt:
        shoot();
        break;
    }
  }

  private void move() {
    var newLevelModel = movePlayer(levelModel, inputProvider.currentAction);
    if (levelModel == newLevelModel) {
      return;
    }
    ignoreInput = true;
    player.GetComponent<Animator>().SetTrigger("playerMove");
    var newPosition = this.gameObjectsBoard[newLevelModel.Player.coords.x, newLevelModel.Player.coords.y].transform.position;
    StartCoroutine(player.MoveOverSeconds(newPosition, 1, () => {
      ignoreInput = false;
      newLevelModel = newLevelModel.LevelByPlayerChangedEnergy(0.02f);
      setLevelModel(newLevelModel);
    }));

  }

  private void shoot() {
    GameObject obj;
    if (!inputProvider.playerSelectedObject(out obj) || obj == null) {
      return;
    }

    setLevelModel(levelModel.LevelByPlayerChangedEnergy(-0.05f));
    var hitLocation = shotHitLocation(player.transform.position,
      inputProvider.playerClickLocation(),
      1 << LayerMask.NameToLayer("terrain"),
      hitDeterminer(objectsToArrayIndices, levelModel));

    var start = player.transform.position;
    var end = hitLocation.point;

    Action modelModification = () => { };
    if (hitLocation.collider != null) {
      var hitObject = hitLocation.collider.gameObject;
      foreach (Transform child in hitObject.transform) {
        if (child.gameObject.tag != "monster") {
          continue;
        }
        var coordinates = objectsToArrayIndices[hitObject];
        modelModification = () => {
          causeDamageAtLocation(coordinates, child.gameObject);
        };
      }
    }
    ignoreInput = true;
    StartCoroutine(sendShot(start, end, () => {
      ignoreInput = false;
      modelModification();
    }));
  }

  private void causeDamageAtLocation(Vector2Int coordinates, GameObject hitObject) {
    setLevelModel(levelModel.LevelByHittingPoint(coordinates, 1));

    if (levelModel.tileForPoint(coordinates).monsterModel == null) {
      Destroy(hitObject);
    }
  }

  static IEnumerator sendShot(Vector2 start, Vector2 end, Action completion) {
    var prefab = (GameObject)Resources.Load("Prefabs/Shot");
    var shot = Instantiate(prefab);
    shot.transform.position = start;
    var angle = Mathf.Atan2(end.y - start.y, end.x - start.x) * Mathf.Rad2Deg;
    shot.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    return shot.MoveOverSpeed(end, 8, () => {
      Destroy(shot);
      completion();
     });
  }

  static RaycastHit2D shotHitLocation(Vector2 origin, Vector2 target, LayerMask layer, 
    Func<GameObject, bool> hitDeterminer) {
    var hits = Physics2D.RaycastAll(origin, target - origin, float.MaxValue, layer);
    var first = hits.FirstOrDefault(hit => hitDeterminer(hit.collider.gameObject));
    return first == default(RaycastHit2D) ? hits.Last() : first;
  }

  static Func<GameObject, bool> hitDeterminer(Dictionary<GameObject, Vector2Int> objectToVector2IntMapping, LevelModel levelModel) {
    return gameObject => {
      Vector2Int point;
      if (!objectToVector2IntMapping.TryGetValue(gameObject, out point)) {
        return false;
      }
      var tile = levelModel.tileForPoint(point);
      return !tile.playerShotCanPass;
    };
  }

  static LevelModel movePlayer(LevelModel levelModel, PlayerAction action) {
    Vector2Int moved = levelModel.Player.coords + action.moveDirection();
    Tile tileInLocation = levelModel.tileForPoint(moved);
    if (tileInLocation.enterable) {
      return levelModel.LevelByMovingPlayer(moved);
    }
    return levelModel;
  }

  private static Vector3 centerOfTile(GameObject[,] tiles, Vector2Int coords) {
    return tiles[coords.x, coords.y].transform.position;
  }

  private void setLevelModel(LevelModel newModel) {
    maskingScript.UpdateEnergy(1 - newModel.Player.energy / newModel.Player.maxEnergy);
    maskingScript.UpdateHealth(1 - newModel.Player.health / newModel.Player.maxHealth);
    levelModel = newModel;
  }
}
