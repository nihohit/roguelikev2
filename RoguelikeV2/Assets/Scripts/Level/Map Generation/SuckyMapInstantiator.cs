﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SuckyMapInstantiator : IMapInstantiator {
  private GameObject rockPrefab = (GameObject)Resources.Load("Prefabs/Rock");
  private GameObject grassPrefab = (GameObject)Resources.Load("Prefabs/Grass");


  public GameObject[,] creatObjectsForModel(LevelModel levelModel, GameObject parent) {
    var size = levelModel.Size;

    GameObject[,] board = new GameObject[size.x, size.y];
    Vector2 halfSize = new Vector2(size.x / 2, size.y / 2);

    for (int i = 0; i < size.x; i++) {
      for (int j = 0; j < size.y; j++) {
        board[i, j] = createTileForModel(levelModel.tileForPoint(i, j), halfSize);
        board[i, j].transform.parent = parent.transform;
      }
    }

    return board;
  }

  private GameObject createTileForModel(Tile tileModel, Vector2 mapHalfSize) {
    GameObject tile = null;
    switch (tileModel.tileType) {
      case TileType.Wall:
        tile = GameObject.Instantiate(rockPrefab);
        break;
      case TileType.Empty:
        tile = GameObject.Instantiate(grassPrefab);
        break;
    }
    tile.layer = LayerMask.NameToLayer("terrain");
    tile.name = string.Format("tile {0}.{1}", tileModel.coords.x, tileModel.coords.y);

    var boxCollider = tile.GetComponent<BoxCollider2D>();
    tile.transform.position = new Vector3((tileModel.coords.x - mapHalfSize.x) * boxCollider.size.x, 
      (tileModel.coords.y - mapHalfSize.y) * boxCollider.size.y, 0);

    var mapManager = ZST_MapManager.SharedInstance;
    var smartTile = tile.GetComponent<ZST_SmartTile>();
    mapManager.AddTile(smartTile);

    if (tileModel.monsterModel != null) {
      GameObject monster = createMonsterFromModel(tileModel.monsterModel);
      monster.transform.parent = tile.transform;
      monster.transform.position = tile.transform.position;
    }

    return tile;
  }

  private GameObject createMonsterFromModel(MonsterModel monsterModel) {
    var gameObject = new GameObject();
    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
    spriteRenderer.sprite = Resources.Load<Sprite>("Sprites/" + monsterModel.spriteName);
    gameObject.name = monsterModel.spriteName;
    gameObject.tag = "monster";
    spriteRenderer.sortingLayerName = "Characters";

    return gameObject;
  }
}
