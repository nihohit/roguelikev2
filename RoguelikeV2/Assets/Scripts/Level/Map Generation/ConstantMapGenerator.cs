﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ConstantMapGenerator : IBoardModelGenerator {
  public LevelModel boardModel(int boardSize) {
    Tile[,] boardModel = new Tile[boardSize, boardSize];

    int halfVector2Int = boardSize / 2;

    for (int i = 0; i < boardSize; i++) {
      for (int j = 0; j < boardSize; j++) {
        TileType type = TileType.Empty;
        if (i == 0 || i == boardSize - 1 || j == 0 || j == boardSize - 1 ||
          (i == halfVector2Int && j >= halfVector2Int - 1 && j <= halfVector2Int + 1) ||
          (j == halfVector2Int && i >= halfVector2Int - 1 && i <= halfVector2Int + 1)) {
          type = TileType.Wall;
        }
        boardModel[i, j] = new Tile(type, new Vector2Int(i, j));
      }
    }

    var pointBeforeEnd = boardSize - 2;
    boardModel[1, pointBeforeEnd] = new Tile(boardModel[1, pointBeforeEnd], monsterModel(new Vector2Int(1, pointBeforeEnd)), null);
    boardModel[pointBeforeEnd, 1] = new Tile(boardModel[pointBeforeEnd,1 ], monsterModel(new Vector2Int(pointBeforeEnd, 1)), null);
    boardModel[pointBeforeEnd, pointBeforeEnd] = new Tile(boardModel[pointBeforeEnd, pointBeforeEnd], monsterModel(new Vector2Int(pointBeforeEnd, pointBeforeEnd)), null);

    var initialPosition = new Vector2Int(1, 1);
    var playerModel = new PlayerModel(initialPosition, 1, 1, 1, 1);
    boardModel[initialPosition.x, initialPosition.y] = new Tile(boardModel[initialPosition.x, initialPosition.y], null, playerModel);
    return new LevelModel(playerModel, boardModel);
  }

  private MonsterModel monsterModel(Vector2Int coords) {
    return new MonsterModel(coords, 2, "Biter");
  }
}
