﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IMapInstantiator {
  GameObject[,] creatObjectsForModel(LevelModel levelModel, GameObject parent);
}
