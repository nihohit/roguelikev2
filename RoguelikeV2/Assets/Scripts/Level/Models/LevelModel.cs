﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelModel {
  public PlayerModel Player { get {
      return tileForPoint(playerModelCoords).playerModel;
    } set {
      tiles[playerModelCoords.x, playerModelCoords.y] = new Tile(tiles[playerModelCoords.x, playerModelCoords.y], null, value);
    }
  }
  private readonly Tile[,] tiles;
  private readonly Vector2Int playerModelCoords;

  public Vector2Int Size { get {
      return new Vector2Int(tiles.GetLength(0), tiles.GetLength(1));
    }
  }

  public LevelModel(PlayerModel model, Tile[,] tiles) {
    playerModelCoords = model.coords;
    this.tiles = tiles;
    Player = model;
  }

  public Tile tileForPoint(int x, int y) {
    return tiles[x, y];
  }

  public Tile tileForPoint(Vector2Int point) {
    return tileForPoint(point.x, point.y);
  }

  public LevelModel LevelByMovingPlayer(Vector2Int newCoords) {
    var newPlayerModel = Player.MoveModel(newCoords);
    if (newPlayerModel.Equals(Player)) {
      return this;
    }

    var oldTile = tiles[Player.coords.x, Player.coords.y];
    var newTile = tiles[newCoords.x, newCoords.y];
    Debug.Assert(oldTile.playerModel == Player);
    Debug.Assert(newTile.isEmpty());

    var replacedTiles = new List<Tile>(){
      new Tile(oldTile, null, null),
      new Tile(newTile, null, newPlayerModel)
    };
    var newTiles = tilesByReplacingTiles(replacedTiles);

    return new LevelModel(newPlayerModel, newTiles);
  }

  public LevelModel LevelByHittingPoint(Vector2Int coords, float damage) {
    var tile = tileForPoint(coords);
    if (tile.monsterModel == null && tile.playerModel == null) {
      return this;
    }

    var newMonster = monsterModelWithDamage(tile.monsterModel, damage);
    var newTiles = tilesByReplacingTiles(new Tile(tile, newMonster, null));
    return new LevelModel(Player, newTiles);
  }

  private Tile[,] tilesByReplacingTiles(Tile replacedTile) {
    return tilesByReplacingTiles(new List<Tile>() { replacedTile });
  }

  private Tile[,] tilesByReplacingTiles(IEnumerable<Tile> replacedTiles) {
    Tile[,] newTiles = (Tile[,])tiles.Clone();
    foreach(var tile in replacedTiles) {
      newTiles[tile.coords.x, tile.coords.y] = tile;
    }
    return newTiles;
  }

  private MonsterModel monsterModelWithDamage(MonsterModel oldModel, float damage) {
    if (oldModel.health <= damage) {
      return null;
    }

    return new MonsterModel(oldModel.coords, oldModel.health - damage, oldModel.spriteName);
  }

  public LevelModel LevelByPlayerChangedEnergy(float energyChange) {
    var newPlayerModel = Player.ChangeEnergy(energyChange);
    if (newPlayerModel.Equals(Player)) {
      return this;
    }

    var oldTile = tiles[Player.coords.x, Player.coords.y];
    var newTile = new Tile(oldTile, null, newPlayerModel);

    return new LevelModel(newPlayerModel, tilesByReplacingTiles(new List<Tile>() { newTile }));
  }
}
