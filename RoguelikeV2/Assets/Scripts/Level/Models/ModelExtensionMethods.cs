﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class ModelExtensionMethods {
  public static Vector2Int moveDirection(this PlayerAction action) {
    switch (action) {
      case PlayerAction.MoveDown:
        return Vector2Int.down;
      case PlayerAction.MoveLeft:
        return Vector2Int.left;
      case PlayerAction.MoveRight:
        return Vector2Int.right;
      case PlayerAction.MoveUp:
        return Vector2Int.up;
      default:
        return Vector2Int.zero;
    }
  }


}
