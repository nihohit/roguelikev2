﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum TileType {
  Empty,
  Wall
}

public class Tile {
  public readonly MonsterModel monsterModel;
  public readonly PlayerModel playerModel;
  public readonly TileType tileType;
  public readonly Vector2Int coords;

  public bool enterable {
    get {
      return isEmpty();
    }
  }

  public bool playerShotCanPass {
    get {
      return tileType != TileType.Wall && monsterModel == null;
    }
  }

  public bool isEmpty() {
    return tileType != TileType.Wall && monsterModel == null && playerModel == null;
  }

  public Tile(TileType type, Vector2Int coords) {
    this.tileType = type;
    this.coords = coords;
  }

  public Tile(Tile tile, MonsterModel monsterModel, PlayerModel playerModel) {
    this.tileType = tile.tileType;
    this.coords = tile.coords;
    this.monsterModel = monsterModel;
    this.playerModel = playerModel;
  }
}
