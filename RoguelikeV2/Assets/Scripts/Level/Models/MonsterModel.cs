﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MonsterModel {
  public readonly Vector2Int coords;
  public readonly float health;
  public readonly string spriteName;

  public MonsterModel(Vector2Int coords, float health, string spriteName) {
    this.coords = coords;
    this.health = health;
    this.spriteName = spriteName;
  }
}
