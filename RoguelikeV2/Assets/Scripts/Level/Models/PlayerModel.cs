﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerModel {
  public readonly Vector2Int coords;
  public readonly float health;
  public readonly float energy;
  public readonly float maxHealth;
  public readonly float maxEnergy;

  public PlayerModel(Vector2Int coords, float health, float energy, float maxHealth, float maxEnergy) {
    this.coords = coords;
    this.health = Mathf.Clamp(health, 0, maxHealth);
    this.energy = Mathf.Clamp(energy, 0, maxEnergy);
    this.maxEnergy = maxEnergy;
    this.maxHealth = maxHealth;
    Debug.Assert(this.maxHealth >= this.health);
    Debug.Assert(this.maxEnergy >= this.energy);
  }

  public PlayerModel MoveModel(Vector2Int newCoords) {
    if (newCoords.Equals(coords)) {
      return this;
    }

    return new PlayerModel(newCoords, health, energy, maxHealth, maxEnergy);
  }

  public PlayerModel ChangeEnergy(float energyChange) {
    if (energyChange == 0) {
      return this;
    }

    return new PlayerModel(coords, health, energy + energyChange, maxHealth, maxEnergy);
  }
}
