﻿// Copyright (c) 2018 Shachar Langbeheim. All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIMaskingScript : MonoBehaviour {
  public GameObject energyMaskObject;
  public GameObject healthMaskObject;
  public GameObject airMaskObject;

  private RectTransform energyTransform;
  private RectTransform healthTransform;
  private RectTransform airTransform;

  private Vector2 energyMaskInitialSize;
  private Vector2 healthMaskInitialSize;
  private Vector2 airMaskInitialSize;

  private void Start() {
    energyTransform = energyMaskObject.GetComponent<RectTransform>();
    energyMaskInitialSize = energyTransform.sizeDelta;
    healthTransform = healthMaskObject.GetComponent<RectTransform>();
    healthMaskInitialSize = healthTransform.sizeDelta;
    airTransform = airMaskObject.GetComponent<RectTransform>();
    airMaskInitialSize = airTransform.sizeDelta;
  }

  public void UpdateAir(float ratio) {
    airTransform.sizeDelta = sizeWithAdjustedWidth(airMaskInitialSize, ratio);
  }

  public void UpdateHealth(float ratio) {
    healthTransform.sizeDelta = sizeWithAdjustedWidth(healthMaskInitialSize, ratio);
  }

  public void UpdateEnergy(float ratio) {
    energyTransform.sizeDelta = sizeWithAdjustedWidth(energyMaskInitialSize, ratio);
  }

  Vector2 sizeWithAdjustedWidth(Vector2 size, float newWdith) {
    return new Vector2(size.x * newWdith, size.y);
  }
}
